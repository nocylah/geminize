document
    .querySelector('#settings')
    .addEventListener('click', async _ => await browser.runtime.openOptionsPage());
